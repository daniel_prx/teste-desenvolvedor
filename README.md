# Teste Desenvolvedor Proxion

# Para fazer o teste

- Clone o projeto ou baixe-o;
- Desenvolva a aplicação solicitada;
- Nos envie o .zip do projeto ou dê commit do mesmo em um repositório e nos envie o link.

Boa sorte!

# Objetivo

O candidato deve desenvolver uma aplicação que irá **listar, cadastrar, editar e remover listas de tarefas** utilizando **Windows Forms .NET** e que seja capaz de efetuar as tarefas citadas a seguir neste documento, utilizando os métodos da **API REST** especificados mais a frente.

## Tela de login

- Campo de texto para usuário e senha;
- Botão para tela de criação de usuários.

\* A senha deve ser criptografa em MD5.

## Tela de cadastro de usuários

- Formulário com os seguintes campos: nome de usuário, senha e nome completo.

## Tela principal

- Será aberta após o login com sucesso;
- Nesta tela devem ser listadas todas as listas de tarefas do usuário logado;
- Também deve conter o nome completo do usuário no topo, juntamente com um botão para logout;
- Nela deve ser possível cadastrar novas listas de tarefas, editar, excluir as mesmas e marcar as tarefas como concluídas ou não, além de editar e excluir os itens da lista.

# Estrutura banco de dados

## user

| campos    | tipo                     |
| --------- | ------------------------ |
| id        | Integer (auto increment) |
| username  | Text                     |
| password  | Text                     |
| full_name | Text                     |

## todo_list

| campos  | tipo                     |
| ------- | ------------------------ |
| id      | Integer (auto increment) |
| name    | Text                     |
| user_id | Integer (FK `user.id`)   |

## todo_list_item

| campos       | tipo                        |
| ------------ | --------------------------- |
| id           | Integer (auto increment)    |
| name         | Text                        |
| is_done      | Boolean                     |
| todo_list_id | Integer (FK `todo_list.id`) |

# API

## Endpoint base

https://prx-test.hasura.app/api/rest

## Métodos

### Login

```
Caminho: /login
POST

{
    "username": "String",
    "password": "String"
}

Retorno

{
  "user": [
    {
      "username": "String"
    }
  ]
}

```

### Adicionar lista de tarefas

```
Caminho: /todo/add
POST

{
    "todo_list": {
        "name": "String",
        "user_id": Integer
    }
}

Retorno

{
  "insert_todo_list_one": {
    "id": Integer
  }
}
```

### Editar lista de tarefas

```
Caminho: /todo/edit
POST

{
    "id": Integer,
    "todo_list": {
        "name": "String",
        "user_id": Integer
    }
}

Retorno

{
  "update_todo_list_by_pk": {
    "id": Integer
  }
}
```

### Remover lista de tarefas

```
Caminho: /todo/delete/:id
DELETE

id: Integer

Retorno

{
    "delete_todo_list_by_pk": {
        "id": Integer
    }
}
```

### Recuperar listas por usuário

```
Caminho: /todo/get/:user_id
GET

user_id: Integer

Retorno

{
  "todo_list": [
    {
      "id": Integer,
      "name": "String",
      "todo_list_items": [
        {
          "id": Integer,
          "name": "String",
          "is_done": Boolean
        }
      ]
    }
  ]
}
```

### Adicionar nova tarefa a lista

```
Caminho: /todo/item/add
POST

{
    "item": {
        "name": "String",
        "todo_list_id": "Integer"
    }
}

Retorno

{
  "insert_todo_list_item_one": {
    "id": Integer
  }
}
```

### Editar tarefa da lista

```
Caminho: /todo/item/edit
POST

{
    "id": Integer,
    "item": {
        "name": "String",
        "todo_list_id": "Integer",
        "is_done": Boolean
    }
}

Retorno

{
  "update_todo_list_item_by_pk": {
    "id": Integer
  }
}
```

### Remover tarefa da lista

```
Caminho: /todo/item/delete/:id
DELETE

id: Integer

Retorno

{
    "delete_todo_list_item_by_pk": {
        "id": Integer
    }
}
```

### Alterar status da tarefa

```
Caminho: /todo/item/toggle
POST

{
    "item_id": "Integer",
    "is_done": "Boolean"
}

Retorno

{
  "update_todo_list_item_by_pk": {
    "id": Integer,
    "is_done": Boolean
  }
}
```

### Adicionar novo usuário

```
Caminho: /user/add
POST

{
    "user": {
        "username": "String",
        "password": "String",
        "full_name": "String"
    }
}

Retorno 

{
  "insert_user_one": {
    "id": Integer
  }
}
```

### Recuperar dados de usuário

```
Caminho: /user/get/:username
GET

username: String

Retorno

{
  "user": [
    {
      "id": Integer,
      "username": "String",
      "full_name": "String"
    }
  ]
}
```
